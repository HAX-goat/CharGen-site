var racesList =[
    {name: "M&auml;nniska", stats: {STR: 18, FYS: 18, SMI: 18, INL: 18, MST: 18, MKR: 18, UTS: 18},
    agesAt: [
        {id: 0, beginsAt: 16, endsAt: 20}, 
        {id: 1, beginsAt: 21, endsAt: 40},
        {id: 2, beginsAt: 41, endsAt: 60},
        {id: 3, beginsAt: 61, endsAt: 80}
    ]}, //18 18 18 18 18 18 18
    {name: "Alv", stats: {STR: 15, FYS: 17, SMI: 20, INL: 16, MST: 18, MKR: 20, UTS: 15},
    agesAt: [
        {id: 0, beginsAt: 16, endsAt: 20}, 
        {id: 1, beginsAt: 21, endsAt: 50},
        {id: 2, beginsAt: 51, endsAt: 3000}
    ]},           //15 17 20 16 18 20 15
    {name: "Dv&auml;rg", stats: {STR: 24, FYS: 19, SMI: 13, INL: 20, MST: 19, MKR: 16, UTS: 12},
    agesAt: [
        {id: 0, beginsAt: 25, endsAt: 45}, 
        {id: 1, beginsAt: 46, endsAt: 90},
        {id: 2, beginsAt: 91, endsAt: 135},
        {id: 3, beginsAt: 136, endsAt: 180}
    ]},    //24 19 13 20 19 16 12
    {name: "Pilk", stats: {STR: 12, FYS: 18, SMI: 22, INL: 16, MST: 19, MKR: 18, UTS: 18},
    agesAt: [
        {id: 0, beginsAt: 16, endsAt: 25}, 
        {id: 1, beginsAt: 26, endsAt: 50},
        {id: 2, beginsAt: 51, endsAt: 75},
        {id: 3, beginsAt: 76, endsAt: 100}
    ]}          //12 18 22 16 19 18 18
    
]

var ages=[
    {   // id: 0
        name: "Ung",
        modifiers:{STR:0.95, FYS:1.05, SMI:1.05, INL:1.00, MST:0.95, MKR:1.00, UTS: 1.00, STO: 1.00}, 
        startMoney: 0.5, fvLand: 5, langBonusRead: 0, langBonusWrite: 0, 
        ageBonus: 1, extraProfessionSkills: 1, maxFv: 7
    },
    {   // id: 1
        name: "Mogen",
        modifiers:{STR:1, FYS:1, SMI:1, INL:1, MST:1, MKR:1, UTS: 1, STO: 1}, 
        startMoney: 1, fvLand: 6, langBonusRead: 0, langBonusWrite: 0, 
        ageBonus: 2, extraProfessionSkills: 2, maxFv: 11
    },
    {   // id: 2
        name: "Medel",
        modifiers:{STR:0.9, FYS:0.95, SMI:0.95, INL:1.00, MST:1.05, MKR:1.00, UTS: 1.05, STO: 1.00}, 
        startMoney: 2, fvLand: 7, langBonusRead: 1, langBonusWrite: 0, 
        ageBonus: 3, extraProfessionSkills: 3, maxFv: 13
    },
    {   // id: 3
        name: "Gammal",
        modifiers:{STR:0.8, FYS:0.9, SMI:0.85, INL:1.00, MST:1.1, MKR:1.00, UTS: 1.10, STO: 0.95}, 
        startMoney: 4, fvLand: 8, langBonusRead: 1, langBonusWrite: 1, 
        ageBonus: 4, extraProfessionSkills: 4, maxFv: 16
    }
]
