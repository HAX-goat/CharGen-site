

function SaveCharCookie(objChar){
    var char = char_SerializeChar(objChar);
    //console.log( char );
    document.cookie = "char="+char;
}
function SaveCharString(objChar){
    var char = char_SerializeChar(objChar);
    console.log( char );
    //document.cookie = "char="+char;
}

function char_getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function LoadCharCookie(){
    var cObj = {};
    var charStr = char_getCookie("char");
    char = JSON.parse(charStr);
    cObj.name = char.name       || name;
    cObj.race = char.race       || race;
    cObj.age =  char.age        || age;
    cObj.skills = char.skills   || skills;
    cObj.stats = char.stats     || stats;
    return cObj;
}
function LoadCharString(charStr){
    var cObj = {};
    char = JSON.parse(charStr);
    cObj.name = char.name       || name;
    cObj.race = char.race       || race;
    cObj.age =  char.age        || age;
    cObj.skills = char.skills   || skills;
    cObj.stats = char.stats     || stats;
    return cObj;
}

function char_SerializeChar(charObj){
    return JSON.stringify(charObj);
}

function Character(cName){
    var cObj = {};
    cObj.name = cName;
    cObj.race = {};
    cObj.age = "";
    cObj.skills = [];
    cObj.stats = {};
    return cObj;
}

function char_SetStat(cObj, statName, statPercent){
    cObj.stats[statName] = statPercent;
    return cObj;
}

function char_SetRace(cObj, raceName, raceObject){
    var raceObj = {};
    raceObj.name = raceName;
    raceObj.maxStats = JSON.parse(raceObject).stats;
    raceObj.ages = JSON.parse(raceObject).agesAt;
    cObj.race = raceObj;
    return cObj;
}

function char_GetStat(cObj, statName){
    return cObj.stats[statName];
}

function char_GetAllStats(cObj){
    return cObj.stats;
}

function char_GetRace(cObj){
    return cObj.race;
}

/*
Character.prototype.SaveCookie = saveChar;
Character.prototype.LoadCookie = loadChar;
Character.prototype.SetStat = setStat;
Character.prototype.SetRace = setRace;

Character.prototype.GetStat = getStat;
Character.prototype.GetAllStats = getAllStats;
Character.prototype.GetRace = getRace;
Character.prototype.Serialize = serializeChar;
// Character.prototype.Serialize = serializeChar;
*/