
var exportChar = {};
function Export(char, arrayOfJobs) {
    exportChar = char;
    var w = window.open();
    //var html = $("#toNewWindow").html();
    var html = CharToHtml(char);
    html += JobsToHtml(arrayOfJobs);
    $(w.document.body).html(html);
}
function CharToHtml(char) {
    var html = "<h2>Genererad karaktär</h2>";
    var divOpen = '<div class="{CLASS}" id="{ID}">';
    var divClose = '</div>';

    html += divOpen.replace("{CLASS}", "charName").replace("{ID}", char.name);
    html += "Namn: ";
    html += char.name;
    html += divClose;    
    
    html += divOpen.replace("{CLASS}", "charAge").replace("{ID}", char.name+"Age");
    html += "&Aring;lder: ";
    html += char.age;
    html += divClose;   
    
    html += divOpen.replace("{CLASS}", "charRace").replace("{ID}", char.name+"Race");
    html += "Ras: ";
    html += char_GetRace(char).name;
    html += divClose;

    html += "<hr />";
    html += "<h3>Grundegenskaper</h3>";
    html += "<br />";

    html += divOpen.replace("{CLASS}", "charBaseStats").replace("{ID}", char.name + "Stats");
    var statMulti = {};
    if (char.age && char.age > 0) {
        for (var idx = 0; idx < ages.length; idx++) {
            if (char.age >= char_GetRace(char).ages[idx].beginsAt && char.age <= char_GetRace(char).ages[idx].endsAt) {
                statMulti = ages[char_GetRace(char).ages[idx].id].modifiers;
                break;
            }
        }
    }
    for (var stat in char_GetAllStats(char)) {
        if (char_GetAllStats(char).hasOwnProperty(stat)) {
            html += stat + ": "+ Math.ceil((((char_GetAllStats(char)[stat]) / 100) * char_GetRace(char).maxStats[stat]) * statMulti[stat]) +" ("+ char_GetAllStats(char)[stat] + " %)";
            html += "<br />";
        }
    }
    html += divClose;
    html += "<hr /><h4>"
    html += "F&auml;rdighetspo&auml;ng: ";
    html += (char.age * 2)+(Math.ceil(((char_GetAllStats(char)["INL"]) / 100) * char_GetRace(char).maxStats["INL"]) * 3);
    html += "</h4><hr />";
//(Ålder * 2) + (INL * 3)

    return html;
}

function JobsToHtml(arrayOfJobs){
    var html = "<h3>Potentiella yrken</h3>";
    
    for(var i = 0; i < arrayOfJobs.length; i++){
        var job = arrayOfJobs[i];
        html += "<hr /><h4>";
        html += job.name + " ["+job.category+"]";
        html += "</h4><br />"
        html += "Baskrav: ";
        for(var bs = 0; bs < job.baseStats.length; bs++){
            if(bs>0){
                html += ", ";
            }
            html += job.baseStats[bs];
        }
        html += ", ";
        html += job.baseSkill;
        html += "<br />";
        html += "<h4>Beskrivning</h4>";
        html += job.description;
        html += "<br />";
        html += "<h4>F&auml;rdighetstill&auml;gg</h4>";
        for(var b = 0; b < job.skillBonuses.length; b++){
            if(b>0){
                html += ", ";
            }
            html += job.skillBonuses[b];
        }
        html += "<br />";
        html += "<h4>Dagssold</h4>";
        html += job.dayPay;
        html += "<br />";
        html += "<h4>4a lyckade krav</h4>";
        html += job.criticalSuccess;
        html += "<br />";
        html += "<h4>Startutrustning</h4>";
        for(var g = 0; g < job.startGear.length; g++){
            if(g>0){
                html += ", ";
            }
            html += job.startGear[g];
        }
        html += "<br />";
        html += "<h4>Stridsinsikt</h4>";
        var ageBonus = 0;
        for (var idx = 0; idx < ages.length; idx++) {
                if (exportChar.age >= char_GetRace(exportChar).ages[idx].beginsAt && exportChar.age <= char_GetRace(exportChar).ages[idx].endsAt){
                    ageBonus = ages[char_GetRace(exportChar).ages[idx].id].ageBonus;
                    break;
                }
        }
        html += job.battleInsight.replace("ÅG", ageBonus);
        html += "<br />";
    }

    return html;
}