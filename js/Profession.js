// 
var professions = [
    {
        name: "Soldat",
        baseStats: ["STR", "FYS", "MST"],
        baseSkill: "Stridsunderhåll (5)",
        professions:[
            {
                name: "Milis",
                description: "Vanliga invånare som har som extra tjänst som soldat vid behov. Har ett vanligt yrke på sidan av sin stridsutbildning.",
                skillBonuses:  ["Närstrid", "Hantverk (valfri)", "Dragfordon", "Rida", "Kasta"],
                dayPay: 30,
                criticalSuccess: "Rpn har lyckats bra och blivit erbjuden soldatträning, spelaren kan välja att utbilda sig till soldat inom armen utan kostnad. Utöver detta får Rpn en valfritt enhandsvapen.",
                startGear: ["Slitstarka kläder", "Läderskor", "Penningpung"],
                battleInsight: "1T2-1 + ÅG"
            }
        ]
    }
]