function recountAbsolutes() {
    var char = parseChar();
    var statMulti = {
        STR: 1,
        FYS: 1,
        SMI: 1,
        INL: 1,
        MST: 1,
        MKR: 1,
        UTS: 1
    };
    if (char.age && char.age > 0) {
        for (var idx = 0; idx < ages.length; idx++) {
            if (char.age >= char_GetRace(char).ages[idx].beginsAt && char.age <= char_GetRace(char).ages[idx].endsAt) {
                statMulti = ages[char_GetRace(char).ages[idx].id].modifiers;
                break;
            }
        }
    }

    var statList = JSON.parse($('#raceSelector').val()).stats;
    $('#abs_0').text(Math.ceil(((parseInt($('#value_0').text()) / 100) * statList.STR) * statMulti.STR));
    $('#abs_0').append(' <span class="unimportant other">' + Math.ceil(((parseInt($('#value_0').text()) / 100) * statList.STR)) + ' f&ouml;re &aring;ldersmodifikation (' + statMulti.STR * 100 + ' %)</span>');

    $('#abs_1').text(Math.ceil(((parseInt($('#value_1').text()) / 100) * statList.FYS) * statMulti.FYS));
    $('#abs_1').append(' <span class="unimportant other">' + Math.ceil(((parseInt($('#value_1').text()) / 100) * statList.FYS)) + ' f&ouml;re &aring;ldersmodifikation (' + statMulti.FYS * 100 + ' %)</span>');

    $('#abs_2').text(Math.ceil(((parseInt($('#value_2').text()) / 100) * statList.SMI) * statMulti.SMI));
    $('#abs_2').append(' <span class="unimportant other">' + Math.ceil(((parseInt($('#value_2').text()) / 100) * statList.SMI)) + ' f&ouml;re &aring;ldersmodifikation (' + statMulti.SMI * 100 + ' %)</span>');

    $('#abs_3').text(Math.ceil(((parseInt($('#value_3').text()) / 100) * statList.INL) * statMulti.INL));
    $('#abs_3').append(' <span class="unimportant other">' + Math.ceil(((parseInt($('#value_3').text()) / 100) * statList.INL)) + ' f&ouml;re &aring;ldersmodifikation (' + statMulti.INL * 100 + ' %)</span>');

    $('#abs_4').text(Math.ceil(((parseInt($('#value_4').text()) / 100) * statList.MST) * statMulti.MST));
    $('#abs_4').append(' <span class="unimportant other">' + Math.ceil(((parseInt($('#value_4').text()) / 100) * statList.MST)) + ' f&ouml;re &aring;ldersmodifikation (' + statMulti.MST * 100 + ' %)</span>');

    $('#abs_5').text(Math.ceil(((parseInt($('#value_5').text()) / 100) * statList.MKR) * statMulti.MKR));
    $('#abs_5').append(' <span class="unimportant other">' + Math.ceil(((parseInt($('#value_5').text()) / 100) * statList.MKR)) + ' f&ouml;re &aring;ldersmodifikation (' + statMulti.MKR * 100 + ' %)</span>');

    $('#abs_6').text(Math.ceil(((parseInt($('#value_6').text()) / 100) * statList.UTS) * statMulti.UTS));
    $('#abs_6').append(' <span class="unimportant other">' + Math.ceil(((parseInt($('#value_6').text()) / 100) * statList.UTS)) + ' f&ouml;re &aring;ldersmodifikation (' + statMulti.UTS * 100 + ' %)</span>');

}

var numReroll = 1;
function postSort(ev, ui) {
    $(".value").attr("id", function (arr) {
        return "value_" + arr;
    });
    $(".rerollButton").attr("id", function (arr) {
        return "rrlBtn_" + arr;
    });

    $(".rerollButton").each(function (idx) {
        $(this).attr('onclick', 'reroll(' + idx + ');');
    });

    recountAbsolutes();
}

function reroll(statIndex) {
    if (numReroll > 2) {
        $(".rerollButton").removeClass("btn-warning");
        $(".rerollButton").addClass("btn-disabled");

        return;
    } else {
        handleReroll(statIndex);
        numReroll++;

        if (numReroll > 2) {
            $(".rerollButton").removeClass("btn-warning");
            $(".rerollButton").addClass("btn-disabled");
        }
        parseChar();
    }
}
function handleReroll(statIndex) {
    var res = roll(2, 10);
    var statVal = -1;
    if (res[0] == 10)
        res[0] = 0;

    if (res[1] == 10)
        res[1] = 0;

    if (res[0] == 0 && res[1] == 0) {
        statVal = 100;
    } else {
        statVal = (res[0] * 10) + res[1];
    }
    var original = parseInt($('#value_' + statIndex).text());
    original += 15;
    original = Math.min(original, 100);
    if (statVal > original) {
        $('#value_' + statIndex).text(statVal);
    } else {
        $('#value_' + statIndex).text(original);
    }
    parseChar();
    recountAbsolutes();
}

function roll(numDice, sides) {
    var diceResults = [];
    for (var i = 0; i < numDice; i++) {
        var res = MersenneTwister.random() * (10000 * sides);
        res = res % sides;
        res += 1;
        diceResults.push(Math.floor(res));
    }
    return diceResults;
}

function genMain() {
    $('#dump').empty();
    numReroll = 1;
    var numMainValues = 7;
    for (var i = 0; i < numMainValues; i++) {
        var res = roll(2, 10);
        var statVal = -1;
        if (res[0] == 10)
            res[0] = 0;

        if (res[1] == 10)
            res[1] = 0;

        if (res[0] == 0 && res[1] == 0) {
            statVal = 100;
        } else {
            statVal = (res[0] * 10) + res[1];
        }
        var statId = '#_stat' + i;
        $('#dump').append('<li><div class="value" id="value_' + i + '">' + statVal + '</div> - <button id="rrlBtn_' + i + '" class="rerollButton btn btn-warning btn-xs" onClick="reroll(' + i + ');">Reroll stat!</button></li>');
        $("#dump").sortable({
            update: postSort
        });
        $("#dump").disableSelection();
    }
    $("#rerollContainer").show();
    recountAbsolutes();
    parseChar();
    $("#ageHolder").css("visibility", "visible");
}

//var char; // = new Character($("#nameInput").val());
function parseChar() {
    var char = Character($("#nameInput").val());
    char = char_SetStat(char, "STR", $("#value_0").text());
    char = char_SetStat(char, "FYS", $("#value_1").text());
    char = char_SetStat(char, "SMI", $("#value_2").text());
    char = char_SetStat(char, "INL", $("#value_3").text());
    char = char_SetStat(char, "MST", $("#value_4").text());
    char = char_SetStat(char, "MKR", $("#value_5").text());
    char = char_SetStat(char, "UTS", $("#value_6").text());
    char = char_SetRace(char, $("#raceSelector option:selected").text(), $('#raceSelector').val());
    char.age = $('#ageInput').val();
    addAges(char);
    ageIt(char);
    return char;
}
function saveChar() {
    var char = Character($("#nameInput").val());
    char = char_SetStat(char, "STR", $("#value_0").text());
    char = char_SetStat(char, "FYS", $("#value_1").text());
    char = char_SetStat(char, "SMI", $("#value_2").text());
    char = char_SetStat(char, "INL", $("#value_3").text());
    char = char_SetStat(char, "MST", $("#value_4").text());
    char = char_SetStat(char, "MKR", $("#value_5").text());
    char = char_SetStat(char, "UTS", $("#value_6").text());
    char = char_SetRace(char, $("#raceSelector option:selected").text(), $('#raceSelector').val());
    SaveCharCookie(char);
    addAges(char);
    ageIt(char);
}

function addAges(char) {
    $("#ageGroups").empty();
    for (var idx = 0; idx < char_GetRace(char).ages.length; idx++) {
        var divAgeGroup = "<div id=\"group" + idx + "\">" + char_GetRace(char).ages[idx].beginsAt + "-" + char_GetRace(char).ages[idx].endsAt + ": " + ages[char_GetRace(char).ages[idx].id].name + "</div>";

        $("#ageGroups").append(divAgeGroup);
    }
}

function loadChar() {
    var char = LoadCharCookie();
    $("#value_0").text(char_GetStat(char, "STR"));
    $("#value_1").text(char_GetStat(char, "FYS"));
    $("#value_2").text(char_GetStat(char, "SMI"));
    $("#value_3").text(char_GetStat(char, "INL"));
    $("#value_4").text(char_GetStat(char, "MST"));
    $("#value_5").text(char_GetStat(char, "MKR"));
    $("#value_6").text(char_GetStat(char, "UTS"));
    SaveCharCookie(char);
}

function addSkills() {
    for (var i = 0; i < skills.length; i++) {
        var startCatDiv = '<div id="skillCat_' + skills[i].name + '" class="collapse">';
        var endDiv = '</div>';
        for (var pIdx = 0; pIdx < professions[i].professions.length; pIdx++) {
            var content = '<a class="btn btn-primary" href="#skill_' + skills[i].skills[pIdx].name + '" data-toggle="collapse">' + skills[i].skills[pIdx].name + '</a>' + '<div id="skill_' + skills[i].skills[pIdx].name + '" class="collapse">';
            content += skills[i].skills[pIdx].description;
            content += endDiv;
            startCatDiv += content;
        }
        startCatDiv += endDiv;

        $("#skillSet").append(startCatDiv);
        var btnHtml = '<button class="btn btn-primary" data-toggle="collapse" data-target="#skillCat_' + skills[i].name + '">' + skills[i].name + '</button>';
        $("#skillBtnContainer").append(btnHtml);
    }
}

function addRaces() {
    for (var i = 0; i < racesList.length; i++) {
        $('#raceSelector').append('<option value=' + JSON.stringify(racesList[i]) + '>' + racesList[i].name + '</option>');
    }
    $("#raceSelector").attr('selectedIndex', 0);
    $("#raceSelector option:first-child").attr('selected', 1)
}